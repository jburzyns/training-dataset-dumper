#ifndef HIT_WRITER_HH
#define HIT_WRITER_HH

// Standard Library things
#include <string>
#include <vector>
#include <memory>
 
#include "AthContainers/AuxElement.h"
#include "TVector3.h"

namespace H5 {
  class Group;
}

namespace xAOD {
  class Jet_v1;
  typedef Jet_v1 Jet;
  class TrackMeasurementValidation_v1;
  typedef TrackMeasurementValidation_v1 TrackMeasurementValidation;
  class Vertex_v1;
  typedef Vertex_v1 Vertex;
}

template <typename T>
using Acc = SG::AuxElement::ConstAccessor<T>;

class HitWriterConfig;
class HitOutputWriter;
class HitConsumers;

struct HitOutputs {
  const xAOD::TrackMeasurementValidation* hit;
  TVector3 hitPos;
  const xAOD::Jet* jet;
  const xAOD::Vertex* pv;
};

class HitWriter
{
public:
  HitWriter(
    H5::Group& output_file,
    const HitWriterConfig&);
  ~HitWriter();
  HitWriter(HitWriter&) = delete;
  HitWriter operator=(HitWriter&) = delete;
  HitWriter(HitWriter&&);
  void sortHitsByDR(std::vector<const xAOD::TrackMeasurementValidation*>& hits,
                    const xAOD::Jet& jet,
                    const xAOD::Vertex& vx);
  void write(const std::vector<const xAOD::TrackMeasurementValidation*>& hits,
  	     const xAOD::Jet& jet,
	     const xAOD::Vertex& vx);
  void write_dummy();
private:
  void add_hit_variables(HitConsumers&);
  std::unique_ptr<HitOutputWriter> m_hdf5_hit_writer;
  float m_dR_to_jet;
  bool m_save_endcap_hits;
  bool m_save_only_clean_hits;
  Acc<int> m_bec;
};

#endif
