#!/usr/bin/env bash

set -Eeu


################################################
# mode-based switches
################################################

# config mapping
#
CFG_DIR=$(dirname $(readlink -e ${BASH_SOURCE[0]}))/../../configs
declare -A CONFIGS=(
    [pflow]=${CFG_DIR}/EMPFlow.json
    [pflow]=${CFG_DIR}/EMPFlow.json
    [slim]=${CFG_DIR}/EMPFlowSlim.json
    [truth]=${CFG_DIR}/EMPFlowTruth.json
    [truthjets]=${CFG_DIR}/AktTruthJets.json
    [trackjets]=${CFG_DIR}/TrackJets.json
    [fatjets]=${CFG_DIR}/FatJets.json
    [ca]=${CFG_DIR}/EMPFlowSlim.json
    [softe]=${CFG_DIR}/EMPFlowGN1_el.json
    [minimal]=${CFG_DIR}/minimal.json
    [athena]=${CFG_DIR}/EMPFlow.json
    [retag]=${CFG_DIR}/EMPFlowSlim.json
    [upgrade]=${CFG_DIR}/upgrade_r24.json
    [upgrade-21p9]=${CFG_DIR}/upgrade_r21p9.json
    [trackless]=${CFG_DIR}/TracklessEMPFlow.json
    [trigger]=${CFG_DIR}/trigger_pflow.json
    [trigger-emtopo]=${CFG_DIR}/trigger_emtopo.json
    [trigger-all]=${CFG_DIR}/trigger_all.json
    [trigger-mc21]=${CFG_DIR}/trigger_mc21.json
    [data]=${CFG_DIR}/EMPFlowData.json
    [multi]=${CFG_DIR}/multi.json
    [flow]=${CFG_DIR}/flow.json
    [trigger-trackjet]=${CFG_DIR}/TriggerTrackJets.json
    [trigger-fatjet]=${CFG_DIR}/TriggerFatJets.json
)

# standard samples
TRIGGER_MC21_AOD=r13983/AOD.601229.e8453_e8455_s3873_s3874_r13983.pool.root
TRIGGER_AOD=r14383/AOD.601229.e8453_e8455_s3873_s3874_r14383.pool.root
OFFLINE_PHYSVAL=p5192/DAOD_PHYSVAL.800030.e7954_s3778_r13258_p5192.small.pool.root
OFFLINE_FTAG1=p5192/DAOD_FTAG1.800030.e7954_s3778_r13258_p5192.small.pool.root

declare -A DATAFILES=(
    [pflow]=${OFFLINE_FTAG1}
    [slim]=${OFFLINE_FTAG1}
    [truth]=${OFFLINE_PHYSVAL}
    [truthjets]=${OFFLINE_PHYSVAL}
    [trackjets]=${OFFLINE_FTAG1}
    [fatjets]=p5488/DAOD_FTAG1.801471.e8441_e7400_s3681_r13144_r13146_p5488.small.pool.root
    [ca]=${OFFLINE_FTAG1}
    [softe]=${OFFLINE_PHYSVAL}
    [minimal]=${OFFLINE_FTAG1}
    [athena]=${OFFLINE_FTAG1}
    [retag]=p5488/DAOD_FTAG1.801471.e8441_e7400_s3681_r13144_r13146_p5488.small.pool.root
    [upgrade]=p5608/DAOD_PHYSVAL.601229.e8481_s4038_r14365_r14367_p5608.small.pool.root
    [upgrade-21p9]=r12714/AOD.600012.e8185_s3695_s3700_r12714.pool.root
    [trackless]=${OFFLINE_FTAG1}
    [trigger]=${TRIGGER_AOD}
    [trigger-emtopo]=${TRIGGER_AOD}
    [trigger-all]=${TRIGGER_AOD}
    [trigger-mc21]=${TRIGGER_MC21_AOD}
    [data]=r13546/AOD.00360026.r13546_p5065.pool.root
    [multi]=${OFFLINE_FTAG1}
    [flow]=${OFFLINE_PHYSVAL}
    [trigger-trackjet]=${TRIGGER_MC21_AOD}
    [trigger-fatjet]=r13983/AOD.801169.e8453_e8455_s3873_s3874_r13983.pool.root
)
declare -A TESTS=(
    [pflow]=dump-single-btag
    [slim]=dump-single-btag
    [truth]=dump-single-btag
    [truthjets]=dump-single-btag
    [trackjets]=dump-single-btag
    [fatjets]=dump-single-btag
    [ca]=ca-dump-single-btag
    [softe]=ca-dump-softe
    [minimal]=ca-dump-minimal-btag
    [athena]=ca-dump-single-btag
    [retag]=ca-dump-retag
    [upgrade]=ca-dump-single-btag
    [upgrade-21p9]=ca-dump-upgrade-21p9
    [trackless]=dump-single-btag
    [trigger]=ca-dump-trigger-pflow
    [trigger-emtopo]=ca-dump-trigger-emtopo
    [trigger-all]=ca-dump-trigger-all
    [trigger-mc21]=ca-dump-trigger-all
    [data]=dump-single-btag
    [multi]=ca-dump-multi-config
    [flow]=ca-dump-single-btag
    [trigger-trackjet]=ca-dump-trigger-trackjet
    [trigger-fatjet]=ca-dump-trigger-fatjet
)

################################################
# parse arguments
################################################

ALL_MODES=${!CONFIGS[*]}

print-usage() {
    echo "usage: ${0##*/} [-rpah] [-d <dir>] (${ALL_MODES[*]// /|})" 1>&2
}

usage() {
    print-usage
    exit 1;
}

help() {
    print-usage
    cat <<EOF

The ${0##*/} utility will download a test DAOD file and use it to
produce a training dataset.

Options:
 -d <dir>: specify directory to run in
 -r: build (and run on) a reduced test file
 -p: force dumping at full precision
 -a: enable floating point exception auditing
 -h: print help

If no -d argument is given we'll create one in /tmp and work there.

EOF
    exit 1
}

DIRECTORY=""
DATA_URL=https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/dumper-test-files/-/raw/main/
REDUCE_INPUT=""
FULL_PRECISION=""
AUDIT_FPE=""

while getopts ":d:rpah" o; do
    case "${o}" in
        d) DIRECTORY=${OPTARG} ;;
        r) REDUCE_INPUT=1 ;;
        p) FULL_PRECISION="-p" ;;
        a) AUDIT_FPE="--FPEAuditor" ;;
        h) help ;;
        *) usage ;;
    esac
done
shift $((OPTIND-1))

if (( $# != 1 )) ; then
    usage
fi

MODE=$1


############################################
# Check that all the modes / paths exist
############################################
#
if [[ ! ${CONFIGS[$MODE]+x} ]]; then usage; fi
CFG=${CONFIGS[$MODE]}

if [[ ! ${DATAFILES[$MODE]+x} ]]; then usage; fi
DOWNLOAD_PATH=${DATAFILES[$MODE]}
FILE=${DOWNLOAD_PATH##*/}

if [[ ! ${TESTS[$MODE]+x} ]]; then usage; fi
RUN=${TESTS[$MODE]}


############################################
# Define the run command
############################################

# basic run command
function run-test-basic {
    local CMD="$RUN $FILE -c $CFG $FULL_PRECISION $AUDIT_FPE"
    echo "running ${CMD}"
    ${CMD}
}

# run on reduced input file
function run-test-reduced {
    local REDUCED=DAOD_SMALL.pool.root
    local REDCMD="ca-make-test-file $FILE -c $CFG -o $REDUCED "
    echo "running ${REDCMD}"
    ${REDCMD}
    cat <<EOF

#################### successfully ran reduction ####################
now processing $REDUCED to HDF5
####################################################################

EOF
    local CMD="$RUN $FILE -c $CFG"
    echo "running ${CMD}"
    ${CMD}
}

if [[ ${REDUCE_INPUT} ]] ; then
    if [[ ${MODE} != @(ca|minimal) ]] ; then
        echo "Input reduction is not supported in '${MODE}' mode" >&2
        exit 1
    fi
    RUN_TEST=run-test-reduced
else
    RUN_TEST=run-test-basic
fi


#############################################
# now start doing stuff
#############################################
#
if [[ -z ${DIRECTORY} ]] ; then
    DIRECTORY=$(mktemp -d)
    echo "running in ${DIRECTORY}" >&2
fi

if [[ ! -d ${DIRECTORY} ]]; then
    if [[ -e ${DIRECTORY} ]] ; then
        echo "${DIRECTORY} is not a directory" >&2
        exit 1
    fi
    mkdir ${DIRECTORY}
fi
cd $DIRECTORY

# get files
if [[ ! -f ${FILE} ]] ; then
    echo "getting file ${FILE}" >&2
    curl -s ${DATA_URL}/${DOWNLOAD_PATH} > ${FILE}
fi

# now run the test
${RUN_TEST}

# require some jets were written
echo "========== output summary ==========="
test-output output.h5
